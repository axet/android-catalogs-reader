package com.github.axet.catalogsreader.net;

import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.protocol.HttpContext;

public class HttpProxy implements Proxy {

    public HttpProxy(HttpProxyClient client, String host, String port, String scheme) {
        client.setProxy(host, Integer.valueOf(port), scheme);
    }

    public HttpProxy(HttpProxyClient client, String host, String port, String scheme, String login, String password) {
        client.setProxy(host, Integer.valueOf(port), scheme, login, password);
    }

    @Override
    public void close() {
    }

    @Override
    public void filter(HttpRequest request, HttpContext context) {
    }

}
