package com.github.axet.catalogsreader.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.axet.androidlibrary.widgets.HeaderRecyclerAdapter;
import com.github.axet.androidlibrary.widgets.HeaderRecyclerView;
import com.github.axet.androidlibrary.widgets.WrapperRecyclerAdapter;
import com.github.axet.catalogsreader.R;
import com.github.axet.catalogsreader.activities.MainActivity;
import com.github.axet.catalogsreader.app.CatalogsApplication;

public class NavigatorFragment extends Fragment implements MainActivity.SearchListener {
    public static String TAG = NavigatorFragment.class.getSimpleName();

    public HeaderRecyclerView list;
    public MainActivity.NavigatorInterface nav;

    public static NavigatorFragment newInstance(Uri uri) {
        NavigatorFragment fragment = new NavigatorFragment();
        Bundle args = new Bundle();
        args.putParcelable("uri", uri);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
        MainActivity main = ((MainActivity) getActivity());
        main.getDrawer().updateManager();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        MainActivity main = ((MainActivity) getActivity());
        Bundle args = getArguments();
        Uri uri = args.getParcelable("uri");
        CatalogsApplication app = CatalogsApplication.from(getContext());
        nav = app.engines.get(uri);
        if (list != null && getAdapter() == nav)
            nav.remove(list); // onDestroy never called
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        list = (HeaderRecyclerView) view.findViewById(R.id.list);
        list.setLayoutManager(new GridLayoutManager(getContext(), 1));
        list.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        nav.install(main, list);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        nav.remove(list);
        nav = null;
        list = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        nav.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return nav.onOptionsItemSelected(item);
    }

    public RecyclerView.Adapter getAdapter() {
        RecyclerView.Adapter a = list.getAdapter();
        if (a instanceof WrapperRecyclerAdapter)
            a = ((WrapperRecyclerAdapter) a).getWrappedAdapter();
        if (a instanceof HeaderRecyclerAdapter)
            a = ((HeaderRecyclerAdapter) a).getWrappedAdapter();
        return a;
    }

    public void setEmptyView(View view) {
        RecyclerView.Adapter a = list.getAdapter();
        if (a instanceof HeaderRecyclerAdapter)
            ((HeaderRecyclerAdapter) a).setEmptyView(view);
    }

    @Override
    public void search(String s) {
        nav.search(s);
    }

    @Override
    public void searchClose() {
        nav.searchClose();
    }

    @Override
    public String getHint() {
        return nav.getHint();
    }
}
