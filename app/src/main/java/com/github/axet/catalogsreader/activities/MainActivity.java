package com.github.axet.catalogsreader.activities;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.github.axet.androidlibrary.activities.AppCompatThemeActivity;
import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.AssetsDexLoader;
import com.github.axet.androidlibrary.app.FileTypeDetector;
import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.androidlibrary.preferences.ScreenlockPreference;
import com.github.axet.androidlibrary.services.FileProvider;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.HeaderRecyclerView;
import com.github.axet.androidlibrary.widgets.OpenFileDialog;
import com.github.axet.androidlibrary.widgets.SearchView;
import com.github.axet.androidlibrary.widgets.WebViewCustom;
import com.github.axet.bookreader.app.BookApplication;
import com.github.axet.bookreader.app.Storage;
import com.github.axet.bookreader.widgets.FBReaderView;
import com.github.axet.catalogsreader.R;
import com.github.axet.catalogsreader.app.BooksCatalog;
import com.github.axet.catalogsreader.app.CatalogsApplication;
import com.github.axet.catalogsreader.app.LocalBooksCatalog;
import com.github.axet.catalogsreader.app.NetworkBooksCatalog;
import com.github.axet.catalogsreader.dialogs.OpenIntentDialogFragment;
import com.github.axet.catalogsreader.fragments.LocalLibraryFragment;
import com.github.axet.catalogsreader.fragments.NavigatorFragment;
import com.github.axet.catalogsreader.fragments.NetworkLibraryFragment;
import com.github.axet.catalogsreader.navigators.Search;
import com.github.axet.catalogsreader.widgets.Drawer;

import org.geometerplus.fbreader.fbreader.options.ImageOptions;
import org.geometerplus.fbreader.fbreader.options.MiscOptions;
import org.geometerplus.zlibrary.text.view.ZLTextPosition;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatThemeActivity implements DialogInterface.OnDismissListener, SharedPreferences.OnSharedPreferenceChangeListener {
    public final static String TAG = MainActivity.class.getSimpleName();

    Runnable refresh;
    Runnable refreshUI;

    Handler handler = new Handler();

    Drawer drawer;

    ScreenReceiver screenreceiver;

    String lastSearch;

    public static void openBookReader(final Context context, Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (uri.getScheme().equals(ContentResolver.SCHEME_FILE)) {
            uri = FileProvider.getProvider().share(Storage.getFile(uri));
            intent.setDataAndType(uri, NetworkLibraryFragment.CONTENTTYPE_EPUB);
            FileProvider.grantPermissions(context, intent);
        } else {
            intent.setDataAndType(uri, NetworkLibraryFragment.CONTENTTYPE_EPUB);
        }
        context.startActivity(intent);
    }

    public static void openBook(final Context context, Uri uri, com.github.axet.bookreader.activities.MainActivity.ProgressDialog builder) {
        try {
            Storage storage = new Storage(context) {
                @Override
                public Uri getStoragePath() {
                    File file = context.getExternalCacheDir();
                    if (file == null)
                        file = context.getCacheDir();
                    return Uri.fromFile(file);
                }
            };
            if (builder == null) {
                builder = new com.github.axet.bookreader.activities.MainActivity.ProgressDialog(context);
                builder.build();
                builder.dialog.show();
            }
            loadBook(context, storage, builder, uri);
        } catch (Exception e) {
            ErrorDialog.Error(context, e);
        }
    }

    public static void loadBook(final Context context, Storage storage, final Storage.Book book) {
        LayoutInflater inflater = LayoutInflater.from(context);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        final ArrayList<ZLTextPosition> selected = new ArrayList<>();

        selected.clear();
        selected.add(book.info.position);

        final com.github.axet.bookreader.app.Storage.FBook fbook = storage.read(book);

        final Runnable done = new Runnable() {
            @Override
            public void run() {
                fbook.close();
                book.info.position = selected.get(0);
                openBookReader(context, book.url);
            }
        };

        builder.setTitle(R.string.book_preview);

        View v = inflater.inflate(R.layout.recent, null);

        View controls = v.findViewById(R.id.recent_controls);
        controls.setVisibility(View.GONE);

        final FBReaderView r = (FBReaderView) v.findViewById(R.id.recent_fbview);
        // r.config.setValue(r.app.ViewOptions.ScrollbarType, 0);
        r.config.setValue(r.app.MiscOptions.WordTappingAction, MiscOptions.WordTappingActionEnum.doNothing);
        r.config.setValue(r.app.ImageOptions.TapAction, ImageOptions.TapActionEnum.doNothing);

        SharedPreferences shared = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        String mode = shared.getString(BookApplication.PREFERENCE_VIEW_MODE, "");
        r.setWidget(mode.equals(FBReaderView.Widgets.CONTINUOUS.toString()) ? FBReaderView.Widgets.CONTINUOUS : FBReaderView.Widgets.PAGING);

        r.loadBook(fbook);

        builder.setView(v);

        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                fbook.close();
            }
        });

        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setNeutralButton(R.string.book_open_in_bookreader, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                done.run();
            }
        });

        builder.show();
    }

    public static void loadBook(final Context context, final Storage storage, final com.github.axet.bookreader.activities.MainActivity.ProgressDialog builder, final Uri u) {
        final Handler handler = new Handler(Looper.getMainLooper());
        Thread thread = new Thread("load book") {
            @Override
            public void run() {
                final Thread t = Thread.currentThread();
                builder.dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        t.interrupt();
                    }
                });
                try {
                    final Storage.Book book = storage.load(u, builder.progress);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            loadBook(context, storage, book);
                        }
                    });
                } catch (FileTypeDetector.DownloadInterrupted e) {
                    Log.d(TAG, "interrupted", e);
                } catch (Throwable e) {
                    ErrorDialog.Post(context, e);
                } finally {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            builder.dialog.cancel();
                        }
                    });
                }
            }
        };
        thread.start();
    }

    public static void openTorrent(Context context, Uri uri) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (uri.getScheme().equals(CatalogsApplication.SCHEME_MAGNET))
                intent.setData(uri);
            else
                intent.setDataAndType(uri, HttpClient.CONTENTTYPE_XBITTORRENT);
            context.startActivity(intent);
        } catch (Exception e) {
            ErrorDialog.Error(context, e);
        }
    }

    public static void startActivity(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }

    public interface SearchListener {
        String getHint();

        void search(String s);

        void searchClose();
    }

    public interface NavigatorInterface extends SearchListener {
        void install(MainActivity main, HeaderRecyclerView list);

        boolean onCreateOptionsMenu(Menu menu);

        boolean onOptionsItemSelected(MenuItem item);

        void remove(HeaderRecyclerView list);
    }

    @Override
    public int getAppTheme() {
        return CatalogsApplication.getTheme(this, R.style.AppThemeLight_NoActionBar, R.style.AppThemeDark_NoActionBar);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        setContentView(R.layout.app_bar_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = new Drawer(this, toolbar);

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        screenreceiver = new ScreenReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                super.onReceive(context, intent);
                if (drawer.isDrawerOpen())
                    drawer.closeDrawer();
            }
        };
        screenreceiver.registerReceiver(this);

        invalidateOptionsMenu();

        shared.registerOnSharedPreferenceChangeListener(MainActivity.this);

        // update unread icon after torrents created
        drawer.updateUnread();

        drawer.updateManager();

        openIntent(getIntent());
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem login = menu.findItem(R.id.action_login);
        MenuItem home = menu.findItem(R.id.action_home);
        MenuItem grid = menu.findItem(R.id.action_grid);

        MenuItem search = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                if (lastSearch != null && !lastSearch.isEmpty())
                    searchView.setQuery(lastSearch, false);
                FragmentManager fm = getSupportFragmentManager();
                List<Fragment> ff = fm.getFragments();
                if (ff != null) {
                    for (Fragment f : ff) {
                        if (f != null && f.isVisible() && f instanceof SearchListener) {
                            SearchListener s = (SearchListener) f;
                            searchView.setQueryHint(s.getHint());
                        }
                    }
                }
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public boolean onQueryTextSubmit(String query) {
                lastSearch = query;
                searchView.clearFocus();
                FragmentManager fm = getSupportFragmentManager();
                List<Fragment> ff = fm.getFragments();
                if (ff != null) {
                    for (Fragment f : ff) {
                        if (f != null && f.isVisible() && f instanceof SearchListener) {
                            SearchListener s = (SearchListener) f;
                            s.search(searchView.getQuery().toString());
                        }
                    }
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        searchView.setOnCollapsedListener(new SearchView.OnCollapsedListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onCollapsed() {
                FragmentManager fm = getSupportFragmentManager();
                List<Fragment> ff = fm.getFragments();
                if (ff != null) {
                    for (Fragment f : ff) {
                        if (f != null && f.isVisible() && f instanceof SearchListener) {
                            SearchListener s = (SearchListener) f;
                            s.searchClose();
                        }
                    }
                }
            }
        });
        searchView.setOnCloseButtonListener(new SearchView.OnCloseButtonListener() {
            @Override
            public void onClosed() {
                lastSearch = "";
            }
        });

        home.setVisible(false);
        login.setVisible(false);
        grid.setVisible(false);

        return true;
    }

    public void close() {
        if (drawer != null) {
            drawer.close();
            drawer = null;
        }

        refreshUI = null;

        if (refresh != null) {
            handler.removeCallbacks(refresh);
            refresh = null;
        }

        if (screenreceiver != null) {
            screenreceiver.close();
            screenreceiver = null;
        }

        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        shared.unregisterOnSharedPreferenceChangeListener(MainActivity.this);

        // do not close storage when mainactivity closes. it may be restarted due to theme change.
        // only close it on shutdown()
        // app.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar base clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            drawer.openDrawer();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_about) {
            AboutPreferenceCompat.showDialog(this, R.raw.about);
            return true;
        }

        if (id == R.id.action_magnet) {
            drawer.closeDrawer();
            final OpenFileDialog.EditTextDialog d = new OpenFileDialog.EditTextDialog(MainActivity.this);
            d.setTitle(getString(R.string.add_magnet));
            if (Build.VERSION.SDK_INT >= 11) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = clipboard.getPrimaryClip();
                if (clip != null) {
                    for (int i = 0; i < clip.getItemCount(); i++) {
                        String text = clip.getItemAt(i).getText().toString().trim();
                        if (text.startsWith(CatalogsApplication.SCHEME_MAGNET))
                            d.setText(text);
                    }
                }
            }
            d.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    CatalogsApplication app = CatalogsApplication.from(MainActivity.this);
                    String ff = d.getText();
                    try {
                        app.engines.addManget(MainActivity.this, ff);
                    } catch (RuntimeException e) {
                        ErrorDialog.Error(MainActivity.this, e);
                    }
                    updateUnread();
                }
            });
            AlertDialog b = d.create();

            b.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        if (isFinishing())
            return;

        invalidateOptionsMenu();

        // update if keyguard enabled or not
        drawer.updateManager();

        refreshUI = new Runnable() {
            @Override
            public void run() {
            }
        };

        refresh = new Runnable() {
            @Override
            public void run() {
                handler.removeCallbacks(refresh);
                handler.postDelayed(refresh, AlarmManager.SEC1);

                if (refreshUI != null)
                    refreshUI.run();
            }
        };
        refresh.run();

        ScreenlockPreference.onResume(this, CatalogsApplication.PREFERENCE_SCREENLOCK);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
        refreshUI = null;
        ScreenlockPreference.onPause(this, CatalogsApplication.PREFERENCE_SCREENLOCK);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        ScreenlockPreference.onUserInteraction(this, CatalogsApplication.PREFERENCE_SCREENLOCK);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Drawer.RESULT_ADD_ENGINE:
                drawer.onRequestPermissionsResult(permissions, grantResults);
                break;
            case Drawer.RESULT_ADD_CATALOG:
                drawer.onRequestPermissionsResult(permissions, grantResults);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Drawer.RESULT_ADD_ENGINE:
                drawer.onActivityResult(resultCode, data);
                break;
            case Drawer.RESULT_ADD_CATALOG:
                drawer.onActivityResult(resultCode, data);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen())
            drawer.closeDrawer();
        else
            moveTaskToBack(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");
        close();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }


    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    public void onDismiss(DialogInterface dialogInterface) {
        Fragment f = getCurrentFragment();
        if (f instanceof NavigatorFragment) {
            NavigatorFragment n = (NavigatorFragment) f;
            if (n.nav instanceof DialogInterface.OnDismissListener)
                ((DialogInterface.OnDismissListener) n.nav).onDismiss(dialogInterface);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        openIntent(intent);
    }

    void openIntent(Intent intent) {
        if (intent == null)
            return;

        Uri u = intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (u == null)
            u = intent.getData();
        if (u == null) {
            String t = intent.getStringExtra(Intent.EXTRA_TEXT); // handling SEND intents
            if (t != null && t.startsWith(WebViewCustom.SCHEME_HTTP))
                u = Uri.parse(t);
        }
        if (u == null)
            return;

        if (u.getLastPathSegment().endsWith(Storage.JSON_EXT)) {
            OpenIntentDialogFragment dialog = new OpenIntentDialogFragment();
            Bundle args = new Bundle();
            args.putString("url", u.toString());
            dialog.setArguments(args);
            dialog.show(getSupportFragmentManager(), "");
        } else {
            openBook(this, u, null);
        }
    }

    public boolean active(Object s) {
        Fragment f = getCurrentFragment();
        if (f instanceof NavigatorFragment && s instanceof Search) {
            NavigatorFragment n = (NavigatorFragment) f;
            return n.nav == s;
        }
        if (f instanceof NetworkLibraryFragment && s instanceof BooksCatalog) {
            BooksCatalog k = (BooksCatalog) s;
            return f.getArguments().getString("url").equals(k.url);
        }
        return false;
    }

    public void updateUnread() {
        drawer.updateUnread();
        drawer.updateManager();
    }

    @SuppressLint("RestrictedApi")
    public void show(NavigatorInterface nav) {
        CatalogsApplication app = CatalogsApplication.from(this);
        Uri uri = app.engines.getUri((Search) nav);
        FragmentManager fm = getSupportFragmentManager();
        List<Fragment> ff = fm.getFragments();
        if (ff != null) {
            for (Fragment f : ff) {
                if (f instanceof NavigatorFragment) {
                    if (f.getArguments().getParcelable("uri").equals(uri)) {
                        addFragment(f, NavigatorFragment.TAG).commit();
                        return;
                    }
                }
            }
        }
        addFragment(NavigatorFragment.newInstance(uri), NavigatorFragment.TAG).commit();
    }

    public Drawer getDrawer() {
        return drawer;
    }

    @SuppressLint("RestrictedApi")
    public void popBackStack(String tag, int flags) { // only pop existing TAG
        FragmentManager fm = getSupportFragmentManager();
        if (tag == null) {
            fm.popBackStack(null, flags);
            return;
        }
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            String n = fm.getBackStackEntryAt(i).getName();
            if (n != null && n.equals(tag)) {
                fm.popBackStack(tag, flags);
                return;
            }
        }
    }

    @SuppressLint("RestrictedApi")
    public Fragment getCurrentFragment() {
        FragmentManager fm = getSupportFragmentManager();
        List<Fragment> ff = fm.getFragments();
        if (ff == null)
            return null;
        for (Fragment f : ff) {
            if (f != null && f.isVisible())
                return f;
        }
        return null;
    }

    @SuppressLint("RestrictedApi")
    public Fragment getNavigatorFragment(NavigatorInterface nav) {
        CatalogsApplication app = CatalogsApplication.from(this);
        FragmentManager fm = getSupportFragmentManager();
        List<Fragment> ff = fm.getFragments();
        if (ff != null) {
            for (Fragment f : ff) {
                if (f instanceof NavigatorFragment) {
                    NavigatorFragment n = (NavigatorFragment) f;
                    if (n.nav == nav)
                        return f;
                }
            }
        }
        Uri uri = app.engines.getUri((Search) nav);
        return NavigatorFragment.newInstance(uri);
    }

    public FragmentTransaction addFragment(Fragment f, String tag) {
        return openFragment(f, tag).addToBackStack(tag);
    }

    public FragmentTransaction openFragment(Fragment f, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        return fm.beginTransaction().replace(R.id.main_content, f, tag); // ConstraintLayout
    }

    @SuppressLint("RestrictedApi")
    public void openLibrary(BooksCatalog ct) {
        String n = ct.url;
        FragmentManager fm = getSupportFragmentManager();
        if (ct instanceof LocalBooksCatalog) {
            List<Fragment> ff = fm.getFragments();
            if (ff != null) {
                for (Fragment f : ff) {
                    if (f instanceof LocalLibraryFragment) {
                        if (f.getArguments().getString("url").equals(n)) {
                            addFragment(f, LocalLibraryFragment.TAG).commit();
                            return;
                        }
                    }
                }
            }
            addFragment(LocalLibraryFragment.newInstance(n), LocalLibraryFragment.TAG).commit();
        }
        if (ct instanceof NetworkBooksCatalog) {
            List<Fragment> ff = fm.getFragments();
            if (ff != null) {
                for (Fragment f : ff) {
                    if (f instanceof NetworkLibraryFragment) {
                        if (f.getArguments().getString("url").equals(n)) {
                            addFragment(f, NetworkLibraryFragment.TAG).commit();
                            return;
                        }
                    }
                }
            }
            addFragment(NetworkLibraryFragment.newInstance(n), NetworkLibraryFragment.TAG).commit();
        }
    }

    public void loadCatalog(Uri u, final Runnable success) { // uri starts with "catalog://". call on thread
        Uri.Builder b = u.buildUpon();
        b.scheme(WebViewCustom.SCHEME_HTTP);
        final BooksCatalog ct = drawer.catalogs.load(b.build());
        drawer.catalogs.save();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                drawer.updateManager();
                openLibrary(ct);
                if (success != null)
                    success.run();
            }
        });
        return;
    }

    public void loadCatalog(String u, JSONObject json, final Runnable success) { // uri starts with "catalog://". call on thread
        JSONObject j = new JSONObject();
        try {
            j.put("map", json); // convert to internal json format
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        final BooksCatalog ct = drawer.catalogs.load(u, j);
        drawer.catalogs.save();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                drawer.updateManager();
                openLibrary(ct);
                if (success != null)
                    success.run();
            }
        });
        return;
    }
}
