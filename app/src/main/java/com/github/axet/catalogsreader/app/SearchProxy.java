package com.github.axet.catalogsreader.app;

public class SearchProxy {
    public String name;
    public String scheme;
    public String host;
    public String port;
    public String login;
    public String password;

    public SearchProxy() {
    }

    public SearchProxy(String n, String s, String h, String p) {
        name = n;
        scheme = s;
        host = h;
        port = p;
    }

    public SearchProxy(String n, String s, String h, String p, String l, String pp) {
        this(n, s, h, p);
        login = l;
        password = pp;
    }
}
