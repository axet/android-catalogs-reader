package com.github.axet.catalogsreader.net;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;

import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.widgets.WebViewCustom;
import com.github.axet.catalogsreader.app.CatalogsApplication;
import com.github.axet.catalogsreader.app.SearchEngine;
import com.github.axet.catalogsreader.app.SearchProxy;
import com.github.axet.catalogsreader.fragments.NavigatorFragment;
import com.github.axet.catalogsreader.navigators.Search;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import javax.net.ssl.SSLContext;

import cz.msebera.android.httpclient.HttpClientConnection;
import cz.msebera.android.httpclient.HttpConnectionMetrics;
import cz.msebera.android.httpclient.HttpException;
import cz.msebera.android.httpclient.HttpHost;
import cz.msebera.android.httpclient.HttpRequest;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.ProtocolException;
import cz.msebera.android.httpclient.client.config.RequestConfig;
import cz.msebera.android.httpclient.client.methods.CloseableHttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpRequestBase;
import cz.msebera.android.httpclient.client.methods.HttpUriRequest;
import cz.msebera.android.httpclient.client.protocol.HttpClientContext;
import cz.msebera.android.httpclient.config.Registry;
import cz.msebera.android.httpclient.config.RegistryBuilder;
import cz.msebera.android.httpclient.conn.ClientConnectionManager;
import cz.msebera.android.httpclient.conn.DnsResolver;
import cz.msebera.android.httpclient.conn.socket.ConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.LayeredConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.socket.PlainConnectionSocketFactory;
import cz.msebera.android.httpclient.conn.ssl.NoopHostnameVerifier;
import cz.msebera.android.httpclient.conn.ssl.SSLConnectionSocketFactory;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;
import cz.msebera.android.httpclient.impl.client.LaxRedirectStrategy;
import cz.msebera.android.httpclient.impl.conn.PoolingHttpClientConnectionManager;
import cz.msebera.android.httpclient.impl.conn.SystemDefaultDnsResolver;
import cz.msebera.android.httpclient.protocol.HttpContext;
import cz.msebera.android.httpclient.protocol.HttpRequestExecutor;
import cz.msebera.android.httpclient.ssl.SSLContextBuilder;
import cz.msebera.android.httpclient.ssl.SSLInitializationException;
import cz.msebera.android.httpclient.ssl.TrustStrategy;

public class HttpProxyClient extends HttpClient {
    public static final AtomicLong SENT = new AtomicLong();
    public static final AtomicLong RECV = new AtomicLong();

    String name = "";
    Proxy proxy;
    MySocketFactory http;
    MySSLSocketFactory https;
    Fragment fragment;
    RecyclerView.Adapter adapter = null;
    HttpRequestExecutor executor = new HttpRequestExecutor() {
        @Override
        protected HttpResponse doSendRequest(HttpRequest request, HttpClientConnection conn, HttpContext context) throws IOException, HttpException {
            HttpResponse response = super.doSendRequest(request, conn, context);
            HttpConnectionMetrics metrics = conn.getMetrics();
            SENT.addAndGet(metrics.getSentBytesCount());
            metrics.reset();
            return response;
        }

        @Override
        protected HttpResponse doReceiveResponse(HttpRequest request, HttpClientConnection conn, HttpContext context) throws HttpException, IOException {
            HttpResponse response = super.doReceiveResponse(request, conn, context);
            HttpConnectionMetrics metrics = conn.getMetrics();
            RECV.addAndGet(metrics.getReceivedBytesCount());
            metrics.reset();
            return response;
        }
    };

    public static void clear(HttpRequest request, HttpContext context) {
        if (request instanceof HttpRequestBase) {
            HttpRequestBase m = (HttpRequestBase) request;
            RequestConfig config = filter(m.getConfig());
            if (config != null)
                m.setConfig(config);
        }
        RequestConfig config = filter((RequestConfig) context.getAttribute(HttpClientContext.REQUEST_CONFIG));
        if (config != null)
            context.setAttribute(HttpClientContext.REQUEST_CONFIG, config);
    }

    public static SSLContext createDefaultContext() throws SSLInitializationException {
        try {
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                    return true;
                }
            });
            return builder.build();
        } catch (Exception ex) {
            throw new SSLInitializationException(ex.getMessage(), ex);
        }
    }

    public static SSLConnectionSocketFactory createDefaultFactory() {
        return new SSLConnectionSocketFactory(createDefaultContext(), null, null, NoopHostnameVerifier.INSTANCE);
    }

    public static RequestConfig filter(RequestConfig config) {
        if (config == null)
            return null;
        config = RequestConfig.copy(config).setProxy(null).build();
        return config;
    }

    public class MySocketFactory implements ConnectionSocketFactory {
        ConnectionSocketFactory base;

        public MySocketFactory(ConnectionSocketFactory b) {
            this.base = b;
        }

        @Override
        public Socket createSocket(HttpContext context) throws IOException {
            return base.createSocket(context);
        }

        @Override
        public Socket connectSocket(int connectTimeout, Socket sock, HttpHost host, InetSocketAddress remoteAddress, InetSocketAddress localAddress, HttpContext context) throws IOException {
            return base.connectSocket(connectTimeout, sock, host, remoteAddress, localAddress, context);
        }
    }

    public class MySSLSocketFactory implements LayeredConnectionSocketFactory {
        LayeredConnectionSocketFactory base;

        public MySSLSocketFactory(ConnectionSocketFactory c) {
            base = (LayeredConnectionSocketFactory) c;
        }

        @Override
        public Socket createLayeredSocket(Socket socket, String target, int port, HttpContext context) throws IOException, UnknownHostException {
            return base.createLayeredSocket(socket, target, port, context);
        }

        @Override
        public Socket createSocket(HttpContext context) throws IOException {
            return base.createSocket(context);
        }

        @Override
        public Socket connectSocket(int connectTimeout, Socket sock, HttpHost host, InetSocketAddress remoteAddress, InetSocketAddress localAddress, HttpContext context) throws IOException {
            return base.connectSocket(connectTimeout, sock, host, remoteAddress, localAddress, context);
        }
    }

    public HttpProxyClient() {
    }

    public HttpProxyClient(Fragment f) {
        this.fragment = f;
        if (f instanceof NavigatorFragment) {
            NavigatorFragment n = (NavigatorFragment) fragment;
            adapter = n.getAdapter();
        }
    }

    public HttpProxyClient(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
    }

    public HttpProxyClient(String cookies) {
        super(cookies);
    }

    @Override
    public void create() {
        super.create();
    }

    public void filter(HttpRequest request, HttpContext context) {
        if (proxy == null)
            clear(request, context);
        else
            proxy.filter(request, context);
    }

    @Override
    public RequestConfig build(RequestConfig.Builder builder) {
        builder.setCircularRedirectsAllowed(true);
        return super.build(builder);
    }

    @Override
    protected CloseableHttpClient build(HttpClientBuilder builder) {
        builder.setRedirectStrategy(new LaxRedirectStrategy() {
            @Override
            public HttpUriRequest getRedirect(HttpRequest request, HttpResponse response, HttpContext context) throws ProtocolException {
                HttpUriRequest r = super.getRedirect(request, response, context);
                filter(r, context);
                return r;
            }
        });
        http = new MySocketFactory(PlainConnectionSocketFactory.getSocketFactory());
        https = new MySSLSocketFactory(HttpClient.getSSLSocketFactory());
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register(WebViewCustom.SCHEME_HTTP, http)
                .register(WebViewCustom.SCHEME_HTTPS, https)
                .build();
        DnsResolver dns = new DnsResolver() {
            @Override
            public InetAddress[] resolve(String host) throws UnknownHostException {
                return SystemDefaultDnsResolver.INSTANCE.resolve(host); // allow force ipv4 or ipv6
            }
        };
        PoolingHttpClientConnectionManager conn = new PoolingHttpClientConnectionManager(socketFactoryRegistry, dns);
        builder.setConnectionManager(conn);
        builder.setRequestExecutor(executor);
        return super.build(builder);
    }

    @Override
    public CloseableHttpResponse execute(HttpRequestBase request) {
        filter(request, httpClientContext);
        return super.execute(request);
    }

    public void update(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);

        if (adapter instanceof Search) { // per Fragment proxy
            Search s = (Search) adapter;
            SearchEngine engine = s.getEngine();
            if (engine != null) {
                String name = shared.getString(CatalogsApplication.PREFERENCE_PROXY_PREFIX + engine.getName(), "");
                SearchProxy p = engine.getProxy();
                if (p != null && name.equals(p.name)) {
                    switch (p.scheme) {
                        case WebViewCustom.SCHEME_HTTP:
                        case WebViewCustom.SCHEME_HTTPS:
                            if (p.login != null)
                                updateProxy(new HttpProxy(this, p.host, p.port, p.scheme, p.login, p.password), name);
                            else
                                updateProxy(new HttpProxy(this, p.host, p.port, p.scheme), name);
                            break;
                        case SocksProxy.SOCKS:
                            updateProxy(new SocksProxy(this, p.host, p.port), name);
                            break;
                    }
                    return;
                }
            }
        }

        String name = shared.getString(CatalogsApplication.PREFERENCE_PROXY, ""); // global proxy

        if (name.isEmpty()) {
            clearProxy();
            closeProxyConnectinos();
            return;
        }

        if (!this.name.equals(name)) {
            clearProxy();
            if (name.equals(GoogleProxy.NAME))
                updateProxy(new GoogleProxy(this), name);
            if (name.equals(TorProxy.NAME))
                updateProxy(new TorProxy(context, this), name);
        }
    }

    void updateProxy(Proxy p, String name) {
        this.proxy = p;
        this.name = name;
        closeProxyConnectinos();
    }

    void closeProxyConnectinos() {
        if (httpclient == null)
            return;
        final ClientConnectionManager cm = httpclient.getConnectionManager();
        Thread thread = new Thread("clear sockets") { // network on main thread
            @Override
            public void run() {
                cm.closeIdleConnections(0, TimeUnit.SECONDS); // drop connections pool, clear sock proxy
            }
        };
        thread.start();
    }

    @Override
    public void clearProxy() {
        super.clearProxy();
        name = "";
        if (proxy != null) {
            proxy.close();
            proxy = null;
        }
        http.base = PlainConnectionSocketFactory.getSocketFactory();
        https.base = HttpClient.getSSLSocketFactory();
    }
}
