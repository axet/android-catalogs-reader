Catalogs Reader
===============

*News subscription application*

Combination of e-book catalogs reader and torrent client search engines in one application, which allows you to keep all subscription and proxy settings in one please. Catalogs can be found here:

* <https://axet.gitlab.io/android-catalogs-reader/>

Manual install
==============

    gradle installDebug

Translate
=========

If you want to translate 'Catalogs Reader' to your language  please read following:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

Screenshots
===========

![shot1](/metadata/screenshots/shot1.png)